# Music - Karaoke Tracker

## Things to do
The process should work as follows...

Program Steps
1. Does table main_chart_list exist
   Yes - get last updated date from maintenance table
   No - build base main_chart_list table and set last chart update to 1/1/2010

2. Is our last chart update older than 30 days?
   No - Skip Step 3 (go to 4)

3. hit billboard.com and get the comprehensive chart list.  Go through and see if chart exists in chart table.  IF SO, skip it, IF NOT, add it.

4. Are there any charts that haven't reached the first entry yet? If yes - process that chart




notes:

multiple charts
chart tables
chart results list (linked to actual songs)

what charts are available...
billboard.com/pmc-ajax/charts-fetch-all-chart/...
selected_category-[chartname] / 
chart_type-[weekly / year-end / decade-end]

