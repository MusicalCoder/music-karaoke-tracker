import os
import data.db_session as db_session

def main():
    setup_db()
    pass

def setup_db():
    db_file = os.path.join(
        os.path.dirname(__file__),
        'db',
        'musicdb.sqlite')

    db_session.global_init(db_file)


if __name__ == '__main__':
    main()