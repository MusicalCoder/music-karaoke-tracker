import sqlalchemy as sa
import sqlalchemy.orm as orm
from sqlalchemy.orm import Session
import data.datamodel as dm

__factory = None


def global_init(db_file: str):
    global __factory

    if __factory:
        return


    if not db_file or not db_file.strip():
        raise Exception("You must specify a data file.")

    conn_str = 'sqlite:///' + db_file
    print("Connecting to DB at: {}".format(conn_str))

    engine = sa.create_engine(conn_str, echo=False)
    __factory = orm.sessionmaker(bind=engine)

    dm.SqlAlchemyBase.metadata.create_all(engine)


def create_session() -> Session:
    global __factory
    return __factory()