import sqlalchemy as sa
import sqlalchemy.orm as orm
import sqlalchemy.ext.declarative as dec
from sqlalchemy.sql.expression import null

SqlAlchemyBase = dec.declarative_base()


# charttypes
class ChartType(SqlAlchemyBase):
    __tablename__ = "chart_types"

    ChartTypeID = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    ChartType = sa.Column(sa.String, index=True)
    IsActive = sa.Column(sa.Boolean, default=True)
    HasReachedFirst = sa.Column(sa.Boolean, default=False)

    def __repr__(self) -> str:
        return '<Package [{0}-{1}]>'.format(self.ChartTypeID, self.ChartType)

# # artists
# class Artist(SqlAlchemyBase):
#     __tablename__ = "artists"

#     ArtistID = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
#     ArtistName = sa.Column(sa.String, index=True)
#     SortedArtistName = sa.Column(sa.String, index=True)
#     IsGroup = sa.Column(sa.Boolean, default=False)
#     GroupID = sa.Column(sa.Integer, sa.ForeignKey("artists.ArtistID"), index=True, nullable=True)
#     FirstChartAppearanceDate = sa.Column(sa.DateTime)
#     LastChartAppearanceDate = sa.Column(sa.DateTime)
#     MusicBrainzID = sa.Column(sa.String, index=True, nullable=True)
#     AudioDBID = sa.Column(sa.String, index=True, nullable=True)

#     MemberOf = orm.relationship('Artist', foreign_keys=[GroupID], backref=sa.backref("artists"))

#     def __repr__(self) -> str:
#         return '<Package {0} - {1}>'.format(self.ArtistID, self.ArtistName)

# # artist aliases
# class ArtistAlias(SqlAlchemyBase):
#     __tablename__ = 'artist_alias'

#     ArtistID = sa.Column(sa.Integer, sa.ForeignKey("artists.ArtistID"), primary_key=True)
#     AliasName = sa.Column(sa.String, primary_key=True, unique=True, index=True, nullable=False)

#     def __repr__(self) -> str:
#         return '<Package {0} - {1}>'.format(self.ArtistID, self.AliasName)

# # songs
# class Song(SqlAlchemyBase):
#     __tablename__ = "songs"

#     SongID = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
#     SongName = sa.Column(sa.String, index=True)
#     SortedSongName = sa.Column(sa.String, index=True)
#     PrimaryArtistID = sa.Column(sa.Integer, sa.ForeignKey("artists.ArtistID"), index=True)
#     Lyrics = sa.Column(sa.String)
#     Length = sa.Column(sa.Integer)
#     Description = sa.Column(sa.String)
#     FirstChartAppearanceDate = sa.Column(sa.DateTime)
#     LastChartAppearanceDate = sa.Column(sa.DateTime)
#     MusicBrainzID = sa.Column(sa.String, index=True, nullable=True)
#     AudioDBID = sa.Column(sa.String, index=True, nullable=True)

#     def __repr__(self) -> str:
#         return '<Package {0} - {1}>'.format(self.SongID, self.SortedSongName)

# # song alias
# class SongAlias(SqlAlchemyBase):
#     __tablename__ = 'songs_alias'

#     SongID = sa.Column(sa.Integer, sa.ForeignKey("songs.SongID"), primary_key=True)
#     AliasName = sa.Column(sa.String, primary_key=True, unique=True, index=True, nullable=False)

#     def __repr__(self) -> str:
#         return '<Package {0} - {1}>'.format(self.SongID, self.AliasName)

# # song contributers
# class SongFeaturing(SqlAlchemyBase):
#     __tablename__ = 'songs_contrib_artists'

#     SongID = sa.Column(sa.Integer, sa.ForeignKey("songs.SongID"), primary_key=True)
#     ArtistID = sa.Column(sa.Integer, sa.ForeignKey("artists.ArtistID"), primary_key=True)

#     def __repr__(self) -> str:
#         return '<Package {0} - {1}>'.format(self.SongID, self.ArtistName)

# # albums
# class Album(SqlAlchemyBase):
#     __tablename__ = 'albums'

#     AlbumID = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
#     PrimaryArtistID = sa.Column(sa.Integer, sa.ForeignKey("artists.ArtistID"), index=True)
#     LabelID = sa.Column(sa.Integer)
#     AlbumName = sa.Column(sa.String, index=True)
#     SortedAlbumName = sa.Column(sa.String, index=True)
#     YearReleased = sa.Column(sa.Integer, nullable=True)
#     TrackCount = sa.Column(sa.Integer, nullable=True)
#     AlbumDescription = sa.Column(sa.String)
#     FirstChartAppearanceDate = sa.Column(sa.DateTime)
#     LastChartAppearanceDate = sa.Column(sa.DateTime)
#     MusicBrainzID = sa.Column(sa.String, index=True, nullable=True)
#     DiscogsID = sa.Column(sa.String, index=True, nullable=True)
#     AudioDBID = sa.Column(sa.String, index=True, nullable=True)

#     def __repr__(self) -> str:
#         return '<Package {0} - {1}>'.format(self.AlbumID, self.SortedAlbumName)

# # album alias
# class AlbumAlias(SqlAlchemyBase):
#     __tablename__ = 'artists_alias'

#     AlbumID = sa.Column(sa.Integer, sa.ForeignKey("albums.AlbumID"), primary_key=True)
#     AliasName = sa.Column(sa.String, primary_key=True, unique=True, index=True, nullable=False)

#     def __repr__(self) -> str:
#         return '<Package {0} - {1}>'.format(self.AlbumID, self.AliasName)

# # album tracks
# class AlbumTracks(SqlAlchemyBase):
#     __tablename__ = 'album_track_order'

#     AlbumID = sa.Column(sa.Integer, sa.ForeignKey("albums.AlbumID"), primary_key=True)
#     PlayOrder = sa.Column(sa.Integer, primary_key=True)
#     SongID = sa.Column(sa.Integer, sa.ForeignKey("songs.SongID"))

#     def __repr__(self) -> str:
#         return '<Package {0} - {1}>'.format(self.AlbumID, self.AliasName)


# # charts
# class MainChart(SqlAlchemyBase):
#     __tablename__ = "main_chart_list"

#     MainChartID = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
#     ChartName = sa.Column(sa.String, index=True)
#     APIReferenceName = sa.Column(sa.String, index=True)
#     LastRundate = sa.Column(sa.DateTime, index=True)
#     ChartTypeID = sa.Column(sa.Integer, sa.ForeignKey("chart_types.ChartTypeID"))

#     def __repr__(self) -> str:
#         return '<Package [{0}-{1}]>'.format(self.MainChartID, self.ChartName)
    
# # chart lists
# class ChartListing(SqlAlchemyBase):
#     __tablename__ = "active_chart_listing"

#     ActiveChartID = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
#     MainChartID = sa.Column(sa.Integer, sa.ForeignKey("main_chart_list.MainChartID"), index=True)
#     ChartDate = sa.Column(sa.DateTime)

#     def __repr__(self) -> str:
#         return '<Package [{0}-{1}]>'.format(self.MainChartID, self.ChartName)

# # chart results
# class ChartResultList(SqlAlchemyBase):
#     __tablename__ = "chart_results"

#     MainChartID = sa.Column(sa.Integer, sa.ForeignKey("main_chart_list.MainChartID"), primary_key=True, index=True)
#     ActiveChartID = sa.Column(sa.Integer, sa.ForeignKey("active_chart_listing.ActiveChartID"), primary_key=True, index=True)
#     ChartPosition = sa.Column(sa.Integer, primary_key=True, index=True, nullable=False)
#     SongID = sa.Column(sa.Integer, sa.ForeignKey("songs.SongID"), index=True)
    
#     def __repr__(self) -> str:
#         return '<Package [{0}-{1}]>'.format(self.MainChartID, self.ChartName)

# # maintenance
# class DBMaint(SqlAlchemyBase):
#     __tablename__ = "maintenance"

#     LastMainChartRefresh = sa.Column(sa.DateTime)


#     def __repr__(self) -> str:
#         return '<Package DBMaint - LastRun: {0}>'.format(self.LastMainChartRefresh)
